'''
All rules that modify fasta files are in this file.
#build_fasta_index
#build_fasta_dictionary
'''

rule build_fasta_index:
    input:
        fa=config["genome"] + config["ext"]
    output:
        temp(config["genome"] + config["ext"] + ".fai")
    log:
       "logs/samtools/build_index/build_index.log"
    threads:4
    message: "Executing Samtools faidx with {threads} threads on the following files {input}."
    shell:
        "samtools faidx {input} 2> {log}"

rule build_fasta_dictionary:
    input:
        fa=config["genome"] + config["ext"]
    output:
        temp(config["genome"] + ".dict")
    log:
       "logs/samtools/build_dict/build_dict.log"
    threads:4
    message: "Executing Picard CreateSequenceDictionary with {threads} threads on the following files {input}."
    shell:
        "picard CreateSequenceDictionary R= {input} O= {output} 2> {log}"
