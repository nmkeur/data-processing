'''
Main script to be executed.
Includes all necessary Snakefiles needed
to execute the workflow.
'''

configfile: "config.yaml"
include: "gatk.snakefile"
include: "fasta.snakefile"
include: "bam.snakefile"
include: "bcftools.snakefile"


rule all:
    input:
        "results/summary.vchk"
onsuccess:
    print("Workflow finished, no error")
onerror:
    print("An error occurred, please check the log files.")

