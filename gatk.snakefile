'''
All rules that use gatk as command are in this file.
#call_variants
#merge_variants
#genotype_variants
'''

# Used to create the correct format for merging.
prefix = "-V"
vcf2=expand("{prefix} calls/{sample}.g.vcf", sample=config["samples"], prefix=prefix)

rule call_variants:
    input:
        fa=config["genome"] + config["ext"],
        dict=config["genome"] + ".dict",
        fai=config["genome"] + config["ext"] +  ".fai",
        bam="fixed-rg/{sample}.bam",
        bai="fixed-rg/{sample}.bai"
    output:
        temp("calls/{sample}.g.vcf")
    log:
       "logs/GATK/call_variants/{sample}.log"
    threads:4
    message: "Executing GATK Haplotypecaller with {threads} threads on the following files {input}."
    shell:
       "java -jar ./GATK/GenomeAnalysisTK.jar -T HaplotypeCaller -R {input.fa} -I {input.bam}"
       " -o {output} -ERC GVCF  >> {log} 2>&1"
       
rule merge_variants:
    input:
         fa=config["genome"] + config["ext"],
         fai=config["genome"] + config["ext"] +  ".fai",
         dict=config["genome"] + ".dict",
         vcf=expand("calls/{sample}.g.vcf", sample=config["samples"]),
    output:
        temp("calls/merged_results.vcf")
    log:
       "logs/GATK/merge_variants/all.log"
    threads:4
    message: "Executing GATK CombineGVCFs with {threads} threads on the following files {input}."
    shell:
        "java -jar ./GATK/GenomeAnalysisTK.jar -T CombineGVCFs -R {input.fa} {vcf2} -o {output} >> {log} 2>&1 "
        

rule genotype_variants:
    input:
         fa=config["genome"] + config["ext"],
         fai=config["genome"] + config["ext"] +  ".fai",
         dict=config["genome"] + ".dict",
         mvcf="calls/merged_results.vcf"
    output:
        protected("results/geno_merged_results.vcf")
    log:
       "logs/GATK/genotype_variants/all.log"
    threads:8
    message: "Executing GATK GenotypeGVCFs with {threads} threads on the following files {input}."
    shell:
        "java -jar ./GATK/GenomeAnalysisTK.jar -T GenotypeGVCFs -R {input.fa}  --variant {input.mvcf} -o {output} >> {log} 2>&1"
