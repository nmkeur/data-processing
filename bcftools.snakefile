rule create_summary:
    input:
        "results/geno_merged_results.vcf"
    output:
        protected("results/summary.vchk")
    log:
       "logs/bcftools/CreateSummary/summary.log"
    threads:2
    message: "Executing VCFtools summary with {threads} threads on the following files {input}."
    shell:
        "bcftools stats {input} >  results/summary.vchk 2> {log}"


