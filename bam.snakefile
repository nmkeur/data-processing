'''
All rules that modify BAM files are in this file.
#replace_rg
#build_bam_index
'''

rule replace_rg:
    input:
        "data/samples/{sample}.bam"
    output:
        temp("fixed-rg/{sample}.bam")
    log:
       "logs/picard/replace_rg/{sample}.log"
    params:
       "RGID={sample} RGLB=lib1 RGPL=illumina RGPU={sample} RGSM={sample} SORT_ORDER=coordinate VALIDATION_STRINGENCY=SILENT"
    threads:4
    message: "Executing Picard Addreadgroup with {threads} threads on the following files {input}."
    wrapper:
        "0.15.4/bio/picard/addorreplacereadgroups"
        
rule build_bam_index:
    input:
        "fixed-rg/{sample}.bam"
    output:
        temp("fixed-rg/{sample}.bai")
    log:
       "logs/picard/BuildBamIndex/{sample}.log"
    threads:4
    message: "Executing Picard  Buildbamindex with {threads} threads on the following files {input}."
    shell:
        "picard BuildBamIndex I= {input} VALIDATION_STRINGENCY=SILENT 2> {log}"

