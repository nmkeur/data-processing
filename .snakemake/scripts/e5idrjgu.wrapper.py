
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x07\x00\x00\x00threadsq\x03K\x01X\x05\x00\x00\x00inputq\x04csnakemake.io\nInputFiles\nq\x05)\x81q\x06XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x07a}q\x08X\x06\x00\x00\x00_namesq\t}q\nsbX\x06\x00\x00\x00paramsq\x0bcsnakemake.io\nParams\nq\x0c)\x81q\r}q\x0eh\t}q\x0fsbX\x03\x00\x00\x00logq\x10csnakemake.io\nLog\nq\x11)\x81q\x12X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x13a}q\x14h\t}q\x15sbX\x04\x00\x00\x00ruleq\x16X\n\x00\x00\x00replace_rgq\x17X\x06\x00\x00\x00outputq\x18csnakemake.io\nOutputFiles\nq\x19)\x81q\x1aX\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x1ba}q\x1ch\t}q\x1dsbX\t\x00\x00\x00resourcesq\x1ecsnakemake.io\nResources\nq\x1f)\x81q (K\x01K\x01e}q!(X\x06\x00\x00\x00_nodesq"K\x01X\x06\x00\x00\x00_coresq#K\x01h\t}q$(h"K\x00N\x86q%h#K\x01N\x86q&uubX\x06\x00\x00\x00configq\'}q((X\x06\x00\x00\x00genomeq)}q*X\x06\x00\x00\x00genomeq+X\x0e\x00\x00\x00data/genome.faq,sX\x07\x00\x00\x00samplesq-}q.X\x12\x00\x00\x00reheader-101948-02q/X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq0suX\t\x00\x00\x00wildcardsq1csnakemake.io\nWildcards\nq2)\x81q3X\x12\x00\x00\x00reheader-101948-02q4a}q5(X\x06\x00\x00\x00sampleq6h4h\t}q7X\x06\x00\x00\x00sampleq8K\x00N\x86q9subub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
