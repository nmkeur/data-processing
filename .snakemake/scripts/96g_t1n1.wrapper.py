
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00configq\x03}q\x04(X\x06\x00\x00\x00genomeq\x05}q\x06X\x06\x00\x00\x00genomeq\x07X\x0e\x00\x00\x00data/genome.faq\x08sX\x07\x00\x00\x00samplesq\t}q\nX\x12\x00\x00\x00reheader-101948-02q\x0bX@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x0csuX\x04\x00\x00\x00ruleq\rX\n\x00\x00\x00replace_rgq\x0eX\x07\x00\x00\x00threadsq\x0fK\x01X\t\x00\x00\x00resourcesq\x10csnakemake.io\nResources\nq\x11)\x81q\x12(K\x01K\x01e}q\x13(X\x06\x00\x00\x00_namesq\x14}q\x15(X\x06\x00\x00\x00_nodesq\x16K\x00N\x86q\x17X\x06\x00\x00\x00_coresq\x18K\x01N\x86q\x19uh\x16K\x01h\x18K\x01ubX\x03\x00\x00\x00logq\x1acsnakemake.io\nLog\nq\x1b)\x81q\x1cX-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x1da}q\x1eh\x14}q\x1fsbX\x05\x00\x00\x00inputq csnakemake.io\nInputFiles\nq!)\x81q"XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq#a}q$h\x14}q%sbX\x06\x00\x00\x00outputq&csnakemake.io\nOutputFiles\nq\')\x81q(X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq)a}q*h\x14}q+sbX\x06\x00\x00\x00paramsq,csnakemake.io\nParams\nq-)\x81q.}q/h\x14}q0sbX\t\x00\x00\x00wildcardsq1csnakemake.io\nWildcards\nq2)\x81q3X\x12\x00\x00\x00reheader-101948-02q4a}q5(h\x14}q6X\x06\x00\x00\x00sampleq7K\x00N\x86q8sX\x06\x00\x00\x00sampleq9h4ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
