
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x05\x00\x00\x00inputq\x03csnakemake.io\nInputFiles\nq\x04)\x81q\x05XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x06\x00\x00\x00paramsq\ncsnakemake.io\nParams\nq\x0b)\x81q\x0cX\xa4\x00\x00\x00RGID=reheader-101948-02 RGLB=lib1 RGPL=illumina RGPU=reheader-101948-02 RGSM=reheader-101948-02 SORT_ORDER=coordinate CREATE_INDEX=true VALIDATION_STRINGENCY=SILENTq\ra}q\x0eh\x08}q\x0fsbX\x03\x00\x00\x00logq\x10csnakemake.io\nLog\nq\x11)\x81q\x12X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x13a}q\x14h\x08}q\x15sbX\x04\x00\x00\x00ruleq\x16X\n\x00\x00\x00replace_rgq\x17X\t\x00\x00\x00wildcardsq\x18csnakemake.io\nWildcards\nq\x19)\x81q\x1aX\x12\x00\x00\x00reheader-101948-02q\x1ba}q\x1c(X\x06\x00\x00\x00sampleq\x1dh\x1bh\x08}q\x1eX\x06\x00\x00\x00sampleq\x1fK\x00N\x86q subX\t\x00\x00\x00resourcesq!csnakemake.io\nResources\nq")\x81q#(K\x01K\x01e}q$(X\x06\x00\x00\x00_coresq%K\x01X\x06\x00\x00\x00_nodesq&K\x01h\x08}q\'(h%K\x00N\x86q(h&K\x01N\x86q)uubX\x06\x00\x00\x00configq*}q+(X\x06\x00\x00\x00genomeq,}q-X\x06\x00\x00\x00genomeq.X\x0e\x00\x00\x00data/genome.faq/sX\x07\x00\x00\x00samplesq0}q1X\x12\x00\x00\x00reheader-101948-02q2X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq3suX\x06\x00\x00\x00outputq4csnakemake.io\nOutputFiles\nq5)\x81q6X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq7a}q8h\x08}q9sbX\x07\x00\x00\x00threadsq:K\x01ub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
