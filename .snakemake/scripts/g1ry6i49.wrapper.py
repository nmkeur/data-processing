
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x04\x00\x00\x00ruleq\x03X\n\x00\x00\x00replace_rgq\x04X\x03\x00\x00\x00logq\x05csnakemake.io\nLog\nq\x06)\x81q\x07X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x08a}q\tX\x06\x00\x00\x00_namesq\n}q\x0bsbX\x06\x00\x00\x00paramsq\x0ccsnakemake.io\nParams\nq\r)\x81q\x0e}q\x0fh\n}q\x10sbX\t\x00\x00\x00resourcesq\x11csnakemake.io\nResources\nq\x12)\x81q\x13(K\x01K\x01e}q\x14(X\x06\x00\x00\x00_nodesq\x15K\x01h\n}q\x16(X\x06\x00\x00\x00_coresq\x17K\x00N\x86q\x18h\x15K\x01N\x86q\x19uh\x17K\x01ubX\x07\x00\x00\x00threadsq\x1aK\x01X\t\x00\x00\x00wildcardsq\x1bcsnakemake.io\nWildcards\nq\x1c)\x81q\x1dX\x12\x00\x00\x00reheader-101948-02q\x1ea}q\x1f(X\x06\x00\x00\x00sampleq h\x1eh\n}q!X\x06\x00\x00\x00sampleq"K\x00N\x86q#subX\x06\x00\x00\x00outputq$csnakemake.io\nOutputFiles\nq%)\x81q&X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\'a}q(h\n}q)sbX\x05\x00\x00\x00inputq*csnakemake.io\nInputFiles\nq+)\x81q,XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq-a}q.h\n}q/sbX\x06\x00\x00\x00configq0}q1(X\x06\x00\x00\x00genomeq2}q3X\x06\x00\x00\x00genomeq4X\x0e\x00\x00\x00data/genome.faq5sX\x07\x00\x00\x00samplesq6}q7X\x12\x00\x00\x00reheader-101948-02q8X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq9suub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
