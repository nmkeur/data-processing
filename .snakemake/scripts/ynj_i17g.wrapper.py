
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00outputq\x03csnakemake.io\nOutputFiles\nq\x04)\x81q\x05X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x06\x00\x00\x00configq\n}q\x0b(X\x06\x00\x00\x00genomeq\x0c}q\rX\x06\x00\x00\x00genomeq\x0eX\x0e\x00\x00\x00data/genome.faq\x0fsX\x07\x00\x00\x00samplesq\x10}q\x11X\x12\x00\x00\x00reheader-101948-02q\x12X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x13suX\x05\x00\x00\x00inputq\x14csnakemake.io\nInputFiles\nq\x15)\x81q\x16XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x17a}q\x18h\x08}q\x19sbX\x04\x00\x00\x00ruleq\x1aX\n\x00\x00\x00replace_rgq\x1bX\x06\x00\x00\x00paramsq\x1ccsnakemake.io\nParams\nq\x1d)\x81q\x1eX7\x00\x00\x00RGLB=lib1 RGPL=illumina RGPU=LL RGSM=reheader-101948-02q\x1fa}q h\x08}q!sbX\t\x00\x00\x00resourcesq"csnakemake.io\nResources\nq#)\x81q$(K\x01K\x01e}q%(X\x06\x00\x00\x00_nodesq&K\x01h\x08}q\'(h&K\x00N\x86q(X\x06\x00\x00\x00_coresq)K\x01N\x86q*uh)K\x01ubX\x07\x00\x00\x00threadsq+K\x01X\t\x00\x00\x00wildcardsq,csnakemake.io\nWildcards\nq-)\x81q.X\x12\x00\x00\x00reheader-101948-02q/a}q0(X\x06\x00\x00\x00sampleq1h/h\x08}q2X\x06\x00\x00\x00sampleq3K\x00N\x86q4subX\x03\x00\x00\x00logq5csnakemake.io\nLog\nq6)\x81q7X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq8a}q9h\x08}q:sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
