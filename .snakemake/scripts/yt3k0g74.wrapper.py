
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\t\x00\x00\x00wildcardsq\x03csnakemake.io\nWildcards\nq\x04)\x81q\x05}q\x06X\x06\x00\x00\x00_namesq\x07}q\x08sbX\x06\x00\x00\x00outputq\tcsnakemake.io\nOutputFiles\nq\n)\x81q\x0bX\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x0ca}q\rh\x07}q\x0esbX\x05\x00\x00\x00inputq\x0fcsnakemake.io\nInputFiles\nq\x10)\x81q\x11XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x12a}q\x13h\x07}q\x14sbX\t\x00\x00\x00resourcesq\x15csnakemake.io\nResources\nq\x16)\x81q\x17(K\x01K\x01e}q\x18(h\x07}q\x19(X\x06\x00\x00\x00_coresq\x1aK\x00N\x86q\x1bX\x06\x00\x00\x00_nodesq\x1cK\x01N\x86q\x1duh\x1cK\x01h\x1aK\x01ubX\x06\x00\x00\x00paramsq\x1ecsnakemake.io\nParams\nq\x1f)\x81q }q!h\x07}q"sbX\x03\x00\x00\x00logq#csnakemake.io\nLog\nq$)\x81q%X1\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.bam.logq&a}q\'h\x07}q(sbX\x06\x00\x00\x00configq)}q*(X\x06\x00\x00\x00genomeq+}q,X\x06\x00\x00\x00genomeq-X\x0e\x00\x00\x00data/genome.faq.sX\x07\x00\x00\x00samplesq/}q0X\x12\x00\x00\x00reheader-101948-02q1X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq2suX\x07\x00\x00\x00threadsq3K\x01X\x04\x00\x00\x00ruleq4X\n\x00\x00\x00replace_rgq5ub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
