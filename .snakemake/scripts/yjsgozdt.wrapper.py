
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x05\x00\x00\x00inputq\x03csnakemake.io\nInputFiles\nq\x04)\x81q\x05XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x04\x00\x00\x00ruleq\nX\n\x00\x00\x00replace_rgq\x0bX\x06\x00\x00\x00outputq\x0ccsnakemake.io\nOutputFiles\nq\r)\x81q\x0eX\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x0fa}q\x10h\x08}q\x11sbX\x03\x00\x00\x00logq\x12csnakemake.io\nLog\nq\x13)\x81q\x14X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x15a}q\x16h\x08}q\x17sbX\t\x00\x00\x00resourcesq\x18csnakemake.io\nResources\nq\x19)\x81q\x1a(K\x01K\x01e}q\x1b(X\x06\x00\x00\x00_nodesq\x1cK\x01h\x08}q\x1d(h\x1cK\x00N\x86q\x1eX\x06\x00\x00\x00_coresq\x1fK\x01N\x86q uh\x1fK\x01ubX\x06\x00\x00\x00configq!}q"(X\x07\x00\x00\x00samplesq#}q$X\x12\x00\x00\x00reheader-101948-02q%X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq&sX\x06\x00\x00\x00genomeq\'}q(X\x06\x00\x00\x00genomeq)X\x0e\x00\x00\x00data/genome.faq*suX\x07\x00\x00\x00threadsq+K\x01X\x06\x00\x00\x00paramsq,csnakemake.io\nParams\nq-)\x81q.}q/h\x08}q0sbX\t\x00\x00\x00wildcardsq1csnakemake.io\nWildcards\nq2)\x81q3X\x12\x00\x00\x00reheader-101948-02q4a}q5(h\x08}q6X\x06\x00\x00\x00sampleq7K\x00N\x86q8sX\x06\x00\x00\x00sampleq9h4ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
