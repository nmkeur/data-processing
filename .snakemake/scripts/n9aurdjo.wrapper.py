
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00logq\x03csnakemake.io\nLog\nq\x04)\x81q\x05X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x07\x00\x00\x00threadsq\nK\x01X\x06\x00\x00\x00paramsq\x0bcsnakemake.io\nParams\nq\x0c)\x81q\r}q\x0eh\x08}q\x0fsbX\x06\x00\x00\x00configq\x10}q\x11(X\x07\x00\x00\x00samplesq\x12}q\x13X\x12\x00\x00\x00reheader-101948-02q\x14X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x15sX\x06\x00\x00\x00genomeq\x16}q\x17X\x06\x00\x00\x00genomeq\x18X\x0e\x00\x00\x00data/genome.faq\x19suX\t\x00\x00\x00wildcardsq\x1acsnakemake.io\nWildcards\nq\x1b)\x81q\x1cX\x12\x00\x00\x00reheader-101948-02q\x1da}q\x1e(h\x08}q\x1fX\x06\x00\x00\x00sampleq K\x00N\x86q!sX\x06\x00\x00\x00sampleq"h\x1dubX\x06\x00\x00\x00outputq#csnakemake.io\nOutputFiles\nq$)\x81q%X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq&a}q\'h\x08}q(sbX\x04\x00\x00\x00ruleq)X\n\x00\x00\x00replace_rgq*X\x05\x00\x00\x00inputq+csnakemake.io\nInputFiles\nq,)\x81q-XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq.a}q/h\x08}q0sbX\t\x00\x00\x00resourcesq1csnakemake.io\nResources\nq2)\x81q3(K\x01K\x01e}q4(X\x06\x00\x00\x00_nodesq5K\x01X\x06\x00\x00\x00_coresq6K\x01h\x08}q7(h5K\x00N\x86q8h6K\x01N\x86q9uubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
