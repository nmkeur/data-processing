
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00logq\x03csnakemake.io\nLog\nq\x04)\x81q\x05X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x06\x00\x00\x00configq\n}q\x0b(X\x06\x00\x00\x00genomeq\x0c}q\rX\x06\x00\x00\x00genomeq\x0eX\x0e\x00\x00\x00data/genome.faq\x0fsX\x07\x00\x00\x00samplesq\x10}q\x11X\x12\x00\x00\x00reheader-101948-02q\x12X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x13suX\t\x00\x00\x00wildcardsq\x14csnakemake.io\nWildcards\nq\x15)\x81q\x16X\x12\x00\x00\x00reheader-101948-02q\x17a}q\x18(h\x08}q\x19X\x06\x00\x00\x00sampleq\x1aK\x00N\x86q\x1bsX\x06\x00\x00\x00sampleq\x1ch\x17ubX\x06\x00\x00\x00outputq\x1dcsnakemake.io\nOutputFiles\nq\x1e)\x81q\x1fX\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq a}q!h\x08}q"sbX\x04\x00\x00\x00ruleq#X\n\x00\x00\x00replace_rgq$X\x07\x00\x00\x00threadsq%K\x01X\x06\x00\x00\x00paramsq&csnakemake.io\nParams\nq\')\x81q(}q)h\x08}q*sbX\x05\x00\x00\x00inputq+csnakemake.io\nInputFiles\nq,)\x81q-XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq.a}q/h\x08}q0sbX\t\x00\x00\x00resourcesq1csnakemake.io\nResources\nq2)\x81q3(K\x01K\x01e}q4(X\x06\x00\x00\x00_coresq5K\x01h\x08}q6(X\x06\x00\x00\x00_nodesq7K\x00N\x86q8h5K\x01N\x86q9uh7K\x01ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
