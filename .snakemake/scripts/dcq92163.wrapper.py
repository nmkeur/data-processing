
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00configq\x03}q\x04(X\x07\x00\x00\x00samplesq\x05}q\x06X\x12\x00\x00\x00reheader-101948-02q\x07X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x08sX\x06\x00\x00\x00genomeq\t}q\nX\x06\x00\x00\x00genomeq\x0bX\x0e\x00\x00\x00data/genome.faq\x0csuX\t\x00\x00\x00wildcardsq\rcsnakemake.io\nWildcards\nq\x0e)\x81q\x0fX\x12\x00\x00\x00reheader-101948-02q\x10a}q\x11(X\x06\x00\x00\x00_namesq\x12}q\x13X\x06\x00\x00\x00sampleq\x14K\x00N\x86q\x15sX\x06\x00\x00\x00sampleq\x16h\x10ubX\x06\x00\x00\x00paramsq\x17csnakemake.io\nParams\nq\x18)\x81q\x19}q\x1ah\x12}q\x1bsbX\x04\x00\x00\x00ruleq\x1cX\n\x00\x00\x00replace_rgq\x1dX\x07\x00\x00\x00threadsq\x1eK\x01X\x05\x00\x00\x00inputq\x1fcsnakemake.io\nInputFiles\nq )\x81q!XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq"a}q#h\x12}q$sbX\x06\x00\x00\x00outputq%csnakemake.io\nOutputFiles\nq&)\x81q\'X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq(a}q)h\x12}q*sbX\x03\x00\x00\x00logq+csnakemake.io\nLog\nq,)\x81q-X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq.a}q/h\x12}q0sbX\t\x00\x00\x00resourcesq1csnakemake.io\nResources\nq2)\x81q3(K\x01K\x01e}q4(h\x12}q5(X\x06\x00\x00\x00_nodesq6K\x00N\x86q7X\x06\x00\x00\x00_coresq8K\x01N\x86q9uh6K\x01h8K\x01ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
