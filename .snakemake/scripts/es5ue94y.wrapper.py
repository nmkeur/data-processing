
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\t\x00\x00\x00resourcesq\x03csnakemake.io\nResources\nq\x04)\x81q\x05(K\x01K\x01e}q\x06(X\x06\x00\x00\x00_namesq\x07}q\x08(X\x06\x00\x00\x00_nodesq\tK\x00N\x86q\nX\x06\x00\x00\x00_coresq\x0bK\x01N\x86q\x0cuh\x0bK\x01h\tK\x01ubX\x07\x00\x00\x00threadsq\rK\x01X\x06\x00\x00\x00outputq\x0ecsnakemake.io\nOutputFiles\nq\x0f)\x81q\x10X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x11a}q\x12h\x07}q\x13sbX\t\x00\x00\x00wildcardsq\x14csnakemake.io\nWildcards\nq\x15)\x81q\x16X\x12\x00\x00\x00reheader-101948-02q\x17a}q\x18(h\x07}q\x19X\x06\x00\x00\x00sampleq\x1aK\x00N\x86q\x1bsX\x06\x00\x00\x00sampleq\x1ch\x17ubX\x03\x00\x00\x00logq\x1dcsnakemake.io\nLog\nq\x1e)\x81q\x1fX-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq a}q!h\x07}q"sbX\x04\x00\x00\x00ruleq#X\n\x00\x00\x00replace_rgq$X\x06\x00\x00\x00configq%}q&(X\x06\x00\x00\x00genomeq\'}q(X\x06\x00\x00\x00genomeq)X\x0e\x00\x00\x00data/genome.faq*sX\x07\x00\x00\x00samplesq+}q,X\x12\x00\x00\x00reheader-101948-02q-X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq.suX\x06\x00\x00\x00paramsq/csnakemake.io\nParams\nq0)\x81q1}q2h\x07}q3sbX\x05\x00\x00\x00inputq4csnakemake.io\nInputFiles\nq5)\x81q6XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq7a}q8h\x07}q9sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
