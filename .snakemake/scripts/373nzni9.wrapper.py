
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00logq\x03csnakemake.io\nLog\nq\x04)\x81q\x05X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x05\x00\x00\x00inputq\ncsnakemake.io\nInputFiles\nq\x0b)\x81q\x0cXA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\ra}q\x0eh\x08}q\x0fsbX\t\x00\x00\x00wildcardsq\x10csnakemake.io\nWildcards\nq\x11)\x81q\x12X\x12\x00\x00\x00reheader-101948-02q\x13a}q\x14(h\x08}q\x15X\x06\x00\x00\x00sampleq\x16K\x00N\x86q\x17sX\x06\x00\x00\x00sampleq\x18h\x13ubX\x06\x00\x00\x00configq\x19}q\x1a(X\x07\x00\x00\x00samplesq\x1b}q\x1cX\x12\x00\x00\x00reheader-101948-02q\x1dX@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x1esX\x06\x00\x00\x00genomeq\x1f}q X\x06\x00\x00\x00genomeq!X\x0e\x00\x00\x00data/genome.faq"suX\x06\x00\x00\x00outputq#csnakemake.io\nOutputFiles\nq$)\x81q%X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq&a}q\'h\x08}q(sbX\x07\x00\x00\x00threadsq)K\x01X\t\x00\x00\x00resourcesq*csnakemake.io\nResources\nq+)\x81q,(K\x01K\x01e}q-(h\x08}q.(X\x06\x00\x00\x00_nodesq/K\x00N\x86q0X\x06\x00\x00\x00_coresq1K\x01N\x86q2uh/K\x01h1K\x01ubX\x06\x00\x00\x00paramsq3csnakemake.io\nParams\nq4)\x81q5}q6h\x08}q7sbX\x04\x00\x00\x00ruleq8X\n\x00\x00\x00replace_rgq9ub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
