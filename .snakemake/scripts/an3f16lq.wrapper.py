
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00paramsq\x03csnakemake.io\nParams\nq\x04)\x81q\x05X|\x00\x00\x00RGID=reheader-101948-02 RGLB=lib1 RGPL=illumina RGPU=reheader-101948-02 RGSM=reheader-101948-02 VALIDATION_STRINGENCY=SILENTq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x07\x00\x00\x00threadsq\nK\x01X\x06\x00\x00\x00configq\x0b}q\x0c(X\x07\x00\x00\x00samplesq\r}q\x0eX\x12\x00\x00\x00reheader-101948-02q\x0fX@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x10sX\x06\x00\x00\x00genomeq\x11}q\x12X\x06\x00\x00\x00genomeq\x13X\x0e\x00\x00\x00data/genome.faq\x14suX\x03\x00\x00\x00logq\x15csnakemake.io\nLog\nq\x16)\x81q\x17X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x18a}q\x19h\x08}q\x1asbX\t\x00\x00\x00resourcesq\x1bcsnakemake.io\nResources\nq\x1c)\x81q\x1d(K\x01K\x01e}q\x1e(X\x06\x00\x00\x00_nodesq\x1fK\x01h\x08}q (h\x1fK\x00N\x86q!X\x06\x00\x00\x00_coresq"K\x01N\x86q#uh"K\x01ubX\t\x00\x00\x00wildcardsq$csnakemake.io\nWildcards\nq%)\x81q&X\x12\x00\x00\x00reheader-101948-02q\'a}q((X\x06\x00\x00\x00sampleq)h\'h\x08}q*X\x06\x00\x00\x00sampleq+K\x00N\x86q,subX\x05\x00\x00\x00inputq-csnakemake.io\nInputFiles\nq.)\x81q/XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq0a}q1h\x08}q2sbX\x04\x00\x00\x00ruleq3X\n\x00\x00\x00replace_rgq4X\x06\x00\x00\x00outputq5csnakemake.io\nOutputFiles\nq6)\x81q7X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq8a}q9h\x08}q:sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
