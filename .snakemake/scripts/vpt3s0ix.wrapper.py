
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x07\x00\x00\x00threadsq\x03K\x01X\t\x00\x00\x00wildcardsq\x04csnakemake.io\nWildcards\nq\x05)\x81q\x06X\x12\x00\x00\x00reheader-101948-02q\x07a}q\x08(X\x06\x00\x00\x00_namesq\t}q\nX\x06\x00\x00\x00sampleq\x0bK\x00N\x86q\x0csX\x06\x00\x00\x00sampleq\rh\x07ubX\x06\x00\x00\x00paramsq\x0ecsnakemake.io\nParams\nq\x0f)\x81q\x10}q\x11h\t}q\x12sbX\x03\x00\x00\x00logq\x13csnakemake.io\nLog\nq\x14)\x81q\x15X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x16a}q\x17h\t}q\x18sbX\x06\x00\x00\x00outputq\x19csnakemake.io\nOutputFiles\nq\x1a)\x81q\x1bX\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x1ca}q\x1dh\t}q\x1esbX\x04\x00\x00\x00ruleq\x1fX\n\x00\x00\x00replace_rgq X\x06\x00\x00\x00configq!}q"(X\x06\x00\x00\x00genomeq#}q$X\x06\x00\x00\x00genomeq%X\x0e\x00\x00\x00data/genome.faq&sX\x07\x00\x00\x00samplesq\'}q(X\x12\x00\x00\x00reheader-101948-02q)X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq*suX\x05\x00\x00\x00inputq+csnakemake.io\nInputFiles\nq,)\x81q-XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq.a}q/h\t}q0sbX\t\x00\x00\x00resourcesq1csnakemake.io\nResources\nq2)\x81q3(K\x01K\x01e}q4(X\x06\x00\x00\x00_nodesq5K\x01h\t}q6(h5K\x00N\x86q7X\x06\x00\x00\x00_coresq8K\x01N\x86q9uh8K\x01ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
