
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00paramsq\x03csnakemake.io\nParams\nq\x04)\x81q\x05}q\x06X\x06\x00\x00\x00_namesq\x07}q\x08sbX\t\x00\x00\x00wildcardsq\tcsnakemake.io\nWildcards\nq\n)\x81q\x0bX\x12\x00\x00\x00reheader-101948-02q\x0ca}q\r(X\x06\x00\x00\x00sampleq\x0eh\x0ch\x07}q\x0fX\x06\x00\x00\x00sampleq\x10K\x00N\x86q\x11subX\x04\x00\x00\x00ruleq\x12X\n\x00\x00\x00replace_rgq\x13X\x07\x00\x00\x00threadsq\x14K\x01X\x03\x00\x00\x00logq\x15csnakemake.io\nLog\nq\x16)\x81q\x17X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x18a}q\x19h\x07}q\x1asbX\x05\x00\x00\x00inputq\x1bcsnakemake.io\nInputFiles\nq\x1c)\x81q\x1dXA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x1ea}q\x1fh\x07}q sbX\t\x00\x00\x00resourcesq!csnakemake.io\nResources\nq")\x81q#(K\x01K\x01e}q$(X\x06\x00\x00\x00_coresq%K\x01X\x06\x00\x00\x00_nodesq&K\x01h\x07}q\'(h%K\x00N\x86q(h&K\x01N\x86q)uubX\x06\x00\x00\x00outputq*csnakemake.io\nOutputFiles\nq+)\x81q,X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq-a}q.h\x07}q/sbX\x06\x00\x00\x00configq0}q1(X\x07\x00\x00\x00samplesq2}q3X\x12\x00\x00\x00reheader-101948-02q4X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq5sX\x06\x00\x00\x00genomeq6}q7X\x06\x00\x00\x00genomeq8X\x0e\x00\x00\x00data/genome.faq9suub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
