
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x05\x00\x00\x00inputq\x03csnakemake.io\nInputFiles\nq\x04)\x81q\x05XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x03\x00\x00\x00logq\ncsnakemake.io\nLog\nq\x0b)\x81q\x0cX1\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.bam.logq\ra}q\x0eh\x08}q\x0fsbX\t\x00\x00\x00wildcardsq\x10csnakemake.io\nWildcards\nq\x11)\x81q\x12}q\x13h\x08}q\x14sbX\x07\x00\x00\x00threadsq\x15K\x01X\x06\x00\x00\x00configq\x16}q\x17(X\x07\x00\x00\x00samplesq\x18}q\x19X\x12\x00\x00\x00reheader-101948-02q\x1aX@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x1bsX\x06\x00\x00\x00genomeq\x1c}q\x1dX\x06\x00\x00\x00genomeq\x1eX\x0e\x00\x00\x00data/genome.faq\x1fsuX\x04\x00\x00\x00ruleq X\n\x00\x00\x00replace_rgq!X\t\x00\x00\x00resourcesq"csnakemake.io\nResources\nq#)\x81q$(K\x01K\x01e}q%(h\x08}q&(X\x06\x00\x00\x00_nodesq\'K\x01N\x86q(X\x06\x00\x00\x00_coresq)K\x00N\x86q*uh\'K\x01h)K\x01ubX\x06\x00\x00\x00paramsq+csnakemake.io\nParams\nq,)\x81q-}q.h\x08}q/sbX\x06\x00\x00\x00outputq0csnakemake.io\nOutputFiles\nq1)\x81q2X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq3a}q4h\x08}q5sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
