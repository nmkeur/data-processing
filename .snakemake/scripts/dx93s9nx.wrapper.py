
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00paramsq\x03csnakemake.io\nParams\nq\x04)\x81q\x05}q\x06X\x06\x00\x00\x00_namesq\x07}q\x08sbX\x03\x00\x00\x00logq\tcsnakemake.io\nLog\nq\n)\x81q\x0bX-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x0ca}q\rh\x07}q\x0esbX\t\x00\x00\x00wildcardsq\x0fcsnakemake.io\nWildcards\nq\x10)\x81q\x11X\x12\x00\x00\x00reheader-101948-02q\x12a}q\x13(X\x06\x00\x00\x00sampleq\x14h\x12h\x07}q\x15X\x06\x00\x00\x00sampleq\x16K\x00N\x86q\x17subX\x05\x00\x00\x00inputq\x18csnakemake.io\nInputFiles\nq\x19)\x81q\x1aXA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x1ba}q\x1ch\x07}q\x1dsbX\x06\x00\x00\x00outputq\x1ecsnakemake.io\nOutputFiles\nq\x1f)\x81q X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq!a}q"h\x07}q#sbX\x06\x00\x00\x00configq$}q%(X\x06\x00\x00\x00genomeq&}q\'X\x06\x00\x00\x00genomeq(X\x0e\x00\x00\x00data/genome.faq)sX\x07\x00\x00\x00samplesq*}q+X\x12\x00\x00\x00reheader-101948-02q,X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq-suX\x07\x00\x00\x00threadsq.K\x01X\x04\x00\x00\x00ruleq/X\n\x00\x00\x00replace_rgq0X\t\x00\x00\x00resourcesq1csnakemake.io\nResources\nq2)\x81q3(K\x01K\x01e}q4(X\x06\x00\x00\x00_nodesq5K\x01X\x06\x00\x00\x00_coresq6K\x01h\x07}q7(h6K\x00N\x86q8h5K\x01N\x86q9uubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
