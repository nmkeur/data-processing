
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\t\x00\x00\x00resourcesq\x03csnakemake.io\nResources\nq\x04)\x81q\x05(K\x01K\x01e}q\x06(X\x06\x00\x00\x00_namesq\x07}q\x08(X\x06\x00\x00\x00_nodesq\tK\x00N\x86q\nX\x06\x00\x00\x00_coresq\x0bK\x01N\x86q\x0cuh\tK\x01h\x0bK\x01ubX\x04\x00\x00\x00ruleq\rX\n\x00\x00\x00replace_rgq\x0eX\x06\x00\x00\x00paramsq\x0fcsnakemake.io\nParams\nq\x10)\x81q\x11XT\x00\x00\x00RGLB=lib1 RGPL=illumina RGPU=LL RGSM=reheader-101948-02 VALIDATION_STRINGENCY=SILENTq\x12a}q\x13h\x07}q\x14sbX\x05\x00\x00\x00inputq\x15csnakemake.io\nInputFiles\nq\x16)\x81q\x17XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x18a}q\x19h\x07}q\x1asbX\x03\x00\x00\x00logq\x1bcsnakemake.io\nLog\nq\x1c)\x81q\x1dX-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x1ea}q\x1fh\x07}q sbX\x07\x00\x00\x00threadsq!K\x01X\x06\x00\x00\x00configq"}q#(X\x07\x00\x00\x00samplesq$}q%X\x12\x00\x00\x00reheader-101948-02q&X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\'sX\x06\x00\x00\x00genomeq(}q)X\x06\x00\x00\x00genomeq*X\x0e\x00\x00\x00data/genome.faq+suX\x06\x00\x00\x00outputq,csnakemake.io\nOutputFiles\nq-)\x81q.X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq/a}q0h\x07}q1sbX\t\x00\x00\x00wildcardsq2csnakemake.io\nWildcards\nq3)\x81q4X\x12\x00\x00\x00reheader-101948-02q5a}q6(h\x07}q7X\x06\x00\x00\x00sampleq8K\x00N\x86q9sX\x06\x00\x00\x00sampleq:h5ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
