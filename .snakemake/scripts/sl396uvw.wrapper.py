
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x05\x00\x00\x00inputq\x03csnakemake.io\nInputFiles\nq\x04)\x81q\x05XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\t\x00\x00\x00wildcardsq\ncsnakemake.io\nWildcards\nq\x0b)\x81q\x0cX\x12\x00\x00\x00reheader-101948-02q\ra}q\x0e(X\x06\x00\x00\x00sampleq\x0fh\rh\x08}q\x10X\x06\x00\x00\x00sampleq\x11K\x00N\x86q\x12subX\t\x00\x00\x00resourcesq\x13csnakemake.io\nResources\nq\x14)\x81q\x15(K\x01K\x01e}q\x16(X\x06\x00\x00\x00_nodesq\x17K\x01h\x08}q\x18(h\x17K\x01N\x86q\x19X\x06\x00\x00\x00_coresq\x1aK\x00N\x86q\x1buh\x1aK\x01ubX\x06\x00\x00\x00configq\x1c}q\x1d(X\x06\x00\x00\x00genomeq\x1e}q\x1fX\x06\x00\x00\x00genomeq X\x0e\x00\x00\x00data/genome.faq!sX\x07\x00\x00\x00samplesq"}q#X\x12\x00\x00\x00reheader-101948-02q$X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq%suX\x06\x00\x00\x00outputq&csnakemake.io\nOutputFiles\nq\')\x81q(X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq)a}q*h\x08}q+sbX\x03\x00\x00\x00logq,csnakemake.io\nLog\nq-)\x81q.X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq/a}q0h\x08}q1sbX\x06\x00\x00\x00paramsq2csnakemake.io\nParams\nq3)\x81q4}q5h\x08}q6sbX\x07\x00\x00\x00threadsq7K\x01X\x04\x00\x00\x00ruleq8X\n\x00\x00\x00replace_rgq9ub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
