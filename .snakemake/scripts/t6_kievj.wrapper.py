
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00paramsq\x03csnakemake.io\nParams\nq\x04)\x81q\x05}q\x06X\x06\x00\x00\x00_namesq\x07}q\x08sbX\x04\x00\x00\x00ruleq\tX\n\x00\x00\x00replace_rgq\nX\t\x00\x00\x00resourcesq\x0bcsnakemake.io\nResources\nq\x0c)\x81q\r(K\x01K\x01e}q\x0e(h\x07}q\x0f(X\x06\x00\x00\x00_nodesq\x10K\x00N\x86q\x11X\x06\x00\x00\x00_coresq\x12K\x01N\x86q\x13uh\x10K\x01h\x12K\x01ubX\t\x00\x00\x00wildcardsq\x14csnakemake.io\nWildcards\nq\x15)\x81q\x16X\x12\x00\x00\x00reheader-101948-02q\x17a}q\x18(h\x07}q\x19X\x06\x00\x00\x00sampleq\x1aK\x00N\x86q\x1bsX\x06\x00\x00\x00sampleq\x1ch\x17ubX\x06\x00\x00\x00outputq\x1dcsnakemake.io\nOutputFiles\nq\x1e)\x81q\x1fX\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq a}q!h\x07}q"sbX\x03\x00\x00\x00logq#csnakemake.io\nLog\nq$)\x81q%X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq&a}q\'h\x07}q(sbX\x06\x00\x00\x00configq)}q*(X\x07\x00\x00\x00samplesq+}q,X\x12\x00\x00\x00reheader-101948-02q-X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq.sX\x06\x00\x00\x00genomeq/}q0X\x06\x00\x00\x00genomeq1X\x0e\x00\x00\x00data/genome.faq2suX\x05\x00\x00\x00inputq3csnakemake.io\nInputFiles\nq4)\x81q5XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq6a}q7h\x07}q8sbX\x07\x00\x00\x00threadsq9K\x01ub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
