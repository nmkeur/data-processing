
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00configq\x03}q\x04(X\x07\x00\x00\x00samplesq\x05}q\x06X\x12\x00\x00\x00reheader-101948-02q\x07X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x08sX\x06\x00\x00\x00genomeq\t}q\nX\x06\x00\x00\x00genomeq\x0bX\x0e\x00\x00\x00data/genome.faq\x0csuX\x03\x00\x00\x00logq\rcsnakemake.io\nLog\nq\x0e)\x81q\x0fX-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x10a}q\x11X\x06\x00\x00\x00_namesq\x12}q\x13sbX\x06\x00\x00\x00paramsq\x14csnakemake.io\nParams\nq\x15)\x81q\x16XG\x00\x00\x00RGLB=lib1 RGPL=illumina RGPU=reheader-101948-02 RGSM=reheader-101948-02q\x17a}q\x18h\x12}q\x19sbX\x05\x00\x00\x00inputq\x1acsnakemake.io\nInputFiles\nq\x1b)\x81q\x1cXA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x1da}q\x1eh\x12}q\x1fsbX\x07\x00\x00\x00threadsq K\x01X\x06\x00\x00\x00outputq!csnakemake.io\nOutputFiles\nq")\x81q#X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq$a}q%h\x12}q&sbX\t\x00\x00\x00resourcesq\'csnakemake.io\nResources\nq()\x81q)(K\x01K\x01e}q*(X\x06\x00\x00\x00_coresq+K\x01h\x12}q,(X\x06\x00\x00\x00_nodesq-K\x00N\x86q.h+K\x01N\x86q/uh-K\x01ubX\t\x00\x00\x00wildcardsq0csnakemake.io\nWildcards\nq1)\x81q2X\x12\x00\x00\x00reheader-101948-02q3a}q4(h\x12}q5X\x06\x00\x00\x00sampleq6K\x00N\x86q7sX\x06\x00\x00\x00sampleq8h3ubX\x04\x00\x00\x00ruleq9X\n\x00\x00\x00replace_rgq:ub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
