
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x04\x00\x00\x00ruleq\x03X\n\x00\x00\x00replace_rgq\x04X\x06\x00\x00\x00outputq\x05csnakemake.io\nOutputFiles\nq\x06)\x81q\x07X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x08a}q\tX\x06\x00\x00\x00_namesq\n}q\x0bsbX\x07\x00\x00\x00threadsq\x0cK\x01X\x03\x00\x00\x00logq\rcsnakemake.io\nLog\nq\x0e)\x81q\x0fX-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x10a}q\x11h\n}q\x12sbX\x05\x00\x00\x00inputq\x13csnakemake.io\nInputFiles\nq\x14)\x81q\x15XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x16a}q\x17h\n}q\x18sbX\t\x00\x00\x00resourcesq\x19csnakemake.io\nResources\nq\x1a)\x81q\x1b(K\x01K\x01e}q\x1c(X\x06\x00\x00\x00_coresq\x1dK\x01h\n}q\x1e(X\x06\x00\x00\x00_nodesq\x1fK\x00N\x86q h\x1dK\x01N\x86q!uh\x1fK\x01ubX\x06\x00\x00\x00configq"}q#(X\x07\x00\x00\x00samplesq$}q%X\x12\x00\x00\x00reheader-101948-02q&X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\'sX\x06\x00\x00\x00genomeq(}q)X\x06\x00\x00\x00genomeq*X\x0e\x00\x00\x00data/genome.faq+suX\x06\x00\x00\x00paramsq,csnakemake.io\nParams\nq-)\x81q.}q/h\n}q0sbX\t\x00\x00\x00wildcardsq1csnakemake.io\nWildcards\nq2)\x81q3X\x12\x00\x00\x00reheader-101948-02q4a}q5(X\x06\x00\x00\x00sampleq6h4h\n}q7X\x06\x00\x00\x00sampleq8K\x00N\x86q9subub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
