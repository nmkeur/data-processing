
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00configq\x03}q\x04(X\x07\x00\x00\x00samplesq\x05}q\x06X\x12\x00\x00\x00reheader-101948-02q\x07X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x08sX\x06\x00\x00\x00genomeq\t}q\nX\x06\x00\x00\x00genomeq\x0bX\x0e\x00\x00\x00data/genome.faq\x0csuX\t\x00\x00\x00wildcardsq\rcsnakemake.io\nWildcards\nq\x0e)\x81q\x0f}q\x10X\x06\x00\x00\x00_namesq\x11}q\x12sbX\x03\x00\x00\x00logq\x13csnakemake.io\nLog\nq\x14)\x81q\x15X1\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.bam.logq\x16a}q\x17h\x11}q\x18sbX\x06\x00\x00\x00paramsq\x19csnakemake.io\nParams\nq\x1a)\x81q\x1b}q\x1ch\x11}q\x1dsbX\x07\x00\x00\x00threadsq\x1eK\x01X\x06\x00\x00\x00outputq\x1fcsnakemake.io\nOutputFiles\nq )\x81q!X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq"a}q#h\x11}q$sbX\x05\x00\x00\x00inputq%csnakemake.io\nInputFiles\nq&)\x81q\'XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq(a}q)h\x11}q*sbX\x04\x00\x00\x00ruleq+X\n\x00\x00\x00replace_rgq,X\t\x00\x00\x00resourcesq-csnakemake.io\nResources\nq.)\x81q/(K\x01K\x01e}q0(h\x11}q1(X\x06\x00\x00\x00_coresq2K\x00N\x86q3X\x06\x00\x00\x00_nodesq4K\x01N\x86q5uh2K\x01h4K\x01ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
