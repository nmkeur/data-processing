
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00logq\x03csnakemake.io\nLog\nq\x04)\x81q\x05X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\t\x00\x00\x00resourcesq\ncsnakemake.io\nResources\nq\x0b)\x81q\x0c(K\x01K\x01e}q\r(h\x08}q\x0e(X\x06\x00\x00\x00_nodesq\x0fK\x00N\x86q\x10X\x06\x00\x00\x00_coresq\x11K\x01N\x86q\x12uh\x0fK\x01h\x11K\x01ubX\x06\x00\x00\x00outputq\x13csnakemake.io\nOutputFiles\nq\x14)\x81q\x15X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x16a}q\x17h\x08}q\x18sbX\x07\x00\x00\x00threadsq\x19K\x01X\x06\x00\x00\x00paramsq\x1acsnakemake.io\nParams\nq\x1b)\x81q\x1c}q\x1dh\x08}q\x1esbX\x04\x00\x00\x00ruleq\x1fX\n\x00\x00\x00replace_rgq X\x06\x00\x00\x00configq!}q"(X\x07\x00\x00\x00samplesq#}q$X\x12\x00\x00\x00reheader-101948-02q%X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq&sX\x06\x00\x00\x00genomeq\'}q(X\x06\x00\x00\x00genomeq)X\x0e\x00\x00\x00data/genome.faq*suX\x05\x00\x00\x00inputq+csnakemake.io\nInputFiles\nq,)\x81q-XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq.a}q/h\x08}q0sbX\t\x00\x00\x00wildcardsq1csnakemake.io\nWildcards\nq2)\x81q3X\x12\x00\x00\x00reheader-101948-02q4a}q5(h\x08}q6X\x06\x00\x00\x00sampleq7K\x00N\x86q8sX\x06\x00\x00\x00sampleq9h4ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
