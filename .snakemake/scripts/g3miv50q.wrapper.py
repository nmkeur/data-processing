
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x05\x00\x00\x00inputq\x03csnakemake.io\nInputFiles\nq\x04)\x81q\x05XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x06\x00\x00\x00paramsq\ncsnakemake.io\nParams\nq\x0b)\x81q\x0c}q\rh\x08}q\x0esbX\x06\x00\x00\x00outputq\x0fcsnakemake.io\nOutputFiles\nq\x10)\x81q\x11X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x12a}q\x13h\x08}q\x14sbX\t\x00\x00\x00resourcesq\x15csnakemake.io\nResources\nq\x16)\x81q\x17(K\x01K\x01e}q\x18(h\x08}q\x19(X\x06\x00\x00\x00_nodesq\x1aK\x00N\x86q\x1bX\x06\x00\x00\x00_coresq\x1cK\x01N\x86q\x1duh\x1cK\x01h\x1aK\x01ubX\t\x00\x00\x00wildcardsq\x1ecsnakemake.io\nWildcards\nq\x1f)\x81q X\x12\x00\x00\x00reheader-101948-02q!a}q"(h\x08}q#X\x06\x00\x00\x00sampleq$K\x00N\x86q%sX\x06\x00\x00\x00sampleq&h!ubX\x04\x00\x00\x00ruleq\'X\n\x00\x00\x00replace_rgq(X\x03\x00\x00\x00logq)csnakemake.io\nLog\nq*)\x81q+X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq,a}q-h\x08}q.sbX\x06\x00\x00\x00configq/}q0(X\x06\x00\x00\x00genomeq1}q2X\x06\x00\x00\x00genomeq3X\x0e\x00\x00\x00data/genome.faq4sX\x07\x00\x00\x00samplesq5}q6X\x12\x00\x00\x00reheader-101948-02q7X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq8suX\x07\x00\x00\x00threadsq9K\x01ub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
