
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00outputq\x03csnakemake.io\nOutputFiles\nq\x04)\x81q\x05X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x03\x00\x00\x00logq\ncsnakemake.io\nLog\nq\x0b)\x81q\x0cX1\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.bam.logq\ra}q\x0eh\x08}q\x0fsbX\t\x00\x00\x00resourcesq\x10csnakemake.io\nResources\nq\x11)\x81q\x12(K\x01K\x01e}q\x13(h\x08}q\x14(X\x06\x00\x00\x00_coresq\x15K\x00N\x86q\x16X\x06\x00\x00\x00_nodesq\x17K\x01N\x86q\x18uh\x15K\x01h\x17K\x01ubX\t\x00\x00\x00wildcardsq\x19csnakemake.io\nWildcards\nq\x1a)\x81q\x1b}q\x1ch\x08}q\x1dsbX\x06\x00\x00\x00paramsq\x1ecsnakemake.io\nParams\nq\x1f)\x81q }q!h\x08}q"sbX\x04\x00\x00\x00ruleq#X\n\x00\x00\x00replace_rgq$X\x06\x00\x00\x00configq%}q&(X\x07\x00\x00\x00samplesq\'}q(X\x12\x00\x00\x00reheader-101948-02q)X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq*sX\x06\x00\x00\x00genomeq+}q,X\x06\x00\x00\x00genomeq-X\x0e\x00\x00\x00data/genome.faq.suX\x07\x00\x00\x00threadsq/K\x01X\x05\x00\x00\x00inputq0csnakemake.io\nInputFiles\nq1)\x81q2XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq3a}q4h\x08}q5sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
