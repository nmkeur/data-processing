
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x07\x00\x00\x00threadsq\x03K\x01X\x05\x00\x00\x00inputq\x04csnakemake.io\nInputFiles\nq\x05)\x81q\x06XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x07a}q\x08X\x06\x00\x00\x00_namesq\t}q\nsbX\x06\x00\x00\x00paramsq\x0bcsnakemake.io\nParams\nq\x0c)\x81q\rX|\x00\x00\x00RGID=reheader-101948-02 RGLB=lib1 RGPL=illumina RGPU=reheader-101948-02 RGSM=reheader-101948-02 VALIDATION_STRINGENCY=SILENTq\x0ea}q\x0fh\t}q\x10sbX\t\x00\x00\x00wildcardsq\x11csnakemake.io\nWildcards\nq\x12)\x81q\x13X\x12\x00\x00\x00reheader-101948-02q\x14a}q\x15(X\x06\x00\x00\x00sampleq\x16h\x14h\t}q\x17X\x06\x00\x00\x00sampleq\x18K\x00N\x86q\x19subX\x06\x00\x00\x00outputq\x1acsnakemake.io\nOutputFiles\nq\x1b)\x81q\x1cX\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x1da}q\x1eh\t}q\x1fsbX\x06\x00\x00\x00configq }q!(X\x06\x00\x00\x00genomeq"}q#X\x06\x00\x00\x00genomeq$X\x0e\x00\x00\x00data/genome.faq%sX\x07\x00\x00\x00samplesq&}q\'X\x12\x00\x00\x00reheader-101948-02q(X\x0b\x00\x00\x00/data/A.bamq)suX\x04\x00\x00\x00ruleq*X\n\x00\x00\x00replace_rgq+X\x03\x00\x00\x00logq,csnakemake.io\nLog\nq-)\x81q.X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq/a}q0h\t}q1sbX\t\x00\x00\x00resourcesq2csnakemake.io\nResources\nq3)\x81q4(K\x01K\x01e}q5(X\x06\x00\x00\x00_coresq6K\x01h\t}q7(h6K\x00N\x86q8X\x06\x00\x00\x00_nodesq9K\x01N\x86q:uh9K\x01ubub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
