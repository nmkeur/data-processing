
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/workflow6/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\t\x00\x00\x00resourcesq\x03csnakemake.io\nResources\nq\x04)\x81q\x05(K\x01K\x01e}q\x06(X\x06\x00\x00\x00_namesq\x07}q\x08(X\x06\x00\x00\x00_nodesq\tK\x00N\x86q\nX\x06\x00\x00\x00_coresq\x0bK\x01N\x86q\x0cuh\tK\x01h\x0bK\x01ubX\t\x00\x00\x00wildcardsq\rcsnakemake.io\nWildcards\nq\x0e)\x81q\x0fX\x01\x00\x00\x00Dq\x10a}q\x11(h\x07}q\x12X\x06\x00\x00\x00sampleq\x13K\x00N\x86q\x14sX\x06\x00\x00\x00sampleq\x15h\x10ubX\x06\x00\x00\x00outputq\x16csnakemake.io\nOutputFiles\nq\x17)\x81q\x18X\x0e\x00\x00\x00fixed-rg/D.bamq\x19a}q\x1ah\x07}q\x1bsbX\x06\x00\x00\x00configq\x1c}q\x1d(X\x06\x00\x00\x00genomeq\x1eX\x06\x00\x00\x00data/Aq\x1fX\x03\x00\x00\x00extq X\x03\x00\x00\x00.faq!X\x07\x00\x00\x00samplesq"}q#(X\x01\x00\x00\x00Bq$X\x0b\x00\x00\x00/data/B.bamq%X\x01\x00\x00\x00Cq&X\x0b\x00\x00\x00/data/C.bamq\'X\x01\x00\x00\x00Eq(X\x0b\x00\x00\x00/data/E.bamq)X\x01\x00\x00\x00Hq*X\x0b\x00\x00\x00/data/H.bamq+h\x10X\x0b\x00\x00\x00/data/D.bamq,X\x01\x00\x00\x00Aq-X\x0b\x00\x00\x00/data/A.bamq.X\x01\x00\x00\x00Iq/X\x0b\x00\x00\x00/data/I.bamq0X\x01\x00\x00\x00Gq1X\x0b\x00\x00\x00/data/G.bamq2X\x01\x00\x00\x00Fq3X\x0b\x00\x00\x00/data/F.bamq4uuX\x07\x00\x00\x00threadsq5K\x01X\x06\x00\x00\x00paramsq6csnakemake.io\nParams\nq7)\x81q8X_\x00\x00\x00RGID=D RGLB=lib1 RGPL=illumina RGPU=D RGSM=D SORT_ORDER=coordinate VALIDATION_STRINGENCY=SILENTq9a}q:h\x07}q;sbX\x04\x00\x00\x00ruleq<X\n\x00\x00\x00replace_rgq=X\x03\x00\x00\x00logq>csnakemake.io\nLog\nq?)\x81q@X\x1c\x00\x00\x00logs/picard/replace_rg/D.logqAa}qBh\x07}qCsbX\x05\x00\x00\x00inputqDcsnakemake.io\nInputFiles\nqE)\x81qFX\x12\x00\x00\x00data/samples/D.bamqGa}qHh\x07}qIsbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
