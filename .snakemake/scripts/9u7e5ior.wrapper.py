
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00outputq\x03csnakemake.io\nOutputFiles\nq\x04)\x81q\x05X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x07\x00\x00\x00threadsq\nK\x01X\x06\x00\x00\x00configq\x0b}q\x0c(X\x06\x00\x00\x00genomeq\r}q\x0eX\x06\x00\x00\x00genomeq\x0fX\x0e\x00\x00\x00data/genome.faq\x10sX\x07\x00\x00\x00samplesq\x11}q\x12X\x12\x00\x00\x00reheader-101948-02q\x13X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x14suX\x05\x00\x00\x00inputq\x15csnakemake.io\nInputFiles\nq\x16)\x81q\x17XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x18a}q\x19h\x08}q\x1asbX\t\x00\x00\x00wildcardsq\x1bcsnakemake.io\nWildcards\nq\x1c)\x81q\x1dX\x12\x00\x00\x00reheader-101948-02q\x1ea}q\x1f(h\x08}q X\x06\x00\x00\x00sampleq!K\x00N\x86q"sX\x06\x00\x00\x00sampleq#h\x1eubX\x04\x00\x00\x00ruleq$X\n\x00\x00\x00replace_rgq%X\t\x00\x00\x00resourcesq&csnakemake.io\nResources\nq\')\x81q((K\x01K\x01e}q)(h\x08}q*(X\x06\x00\x00\x00_coresq+K\x00N\x86q,X\x06\x00\x00\x00_nodesq-K\x01N\x86q.uh+K\x01h-K\x01ubX\x06\x00\x00\x00paramsq/csnakemake.io\nParams\nq0)\x81q1}q2h\x08}q3sbX\x03\x00\x00\x00logq4csnakemake.io\nLog\nq5)\x81q6X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq7a}q8h\x08}q9sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
