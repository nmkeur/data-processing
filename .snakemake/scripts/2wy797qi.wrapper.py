
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00configq\x03}q\x04(X\x06\x00\x00\x00genomeq\x05}q\x06X\x06\x00\x00\x00genomeq\x07X\x0e\x00\x00\x00data/genome.faq\x08sX\x07\x00\x00\x00samplesq\t}q\nX\x12\x00\x00\x00reheader-101948-02q\x0bX@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x0csuX\x04\x00\x00\x00ruleq\rX\n\x00\x00\x00replace_rgq\x0eX\x05\x00\x00\x00inputq\x0fcsnakemake.io\nInputFiles\nq\x10)\x81q\x11XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x12a}q\x13X\x06\x00\x00\x00_namesq\x14}q\x15sbX\t\x00\x00\x00wildcardsq\x16csnakemake.io\nWildcards\nq\x17)\x81q\x18}q\x19h\x14}q\x1asbX\t\x00\x00\x00resourcesq\x1bcsnakemake.io\nResources\nq\x1c)\x81q\x1d(K\x01K\x01e}q\x1e(X\x06\x00\x00\x00_nodesq\x1fK\x01X\x06\x00\x00\x00_coresq K\x01h\x14}q!(h K\x00N\x86q"h\x1fK\x01N\x86q#uubX\x07\x00\x00\x00threadsq$K\x01X\x06\x00\x00\x00outputq%csnakemake.io\nOutputFiles\nq&)\x81q\'X#\x00\x00\x00fixed-rg/reheader-101948-02.bam.bamq(a}q)h\x14}q*sbX\x06\x00\x00\x00paramsq+csnakemake.io\nParams\nq,)\x81q-}q.h\x14}q/sbX\x03\x00\x00\x00logq0csnakemake.io\nLog\nq1)\x81q2X1\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.bam.logq3a}q4h\x14}q5sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
