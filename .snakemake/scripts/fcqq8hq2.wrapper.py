
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00configq\x03}q\x04(X\x07\x00\x00\x00samplesq\x05}q\x06X\x12\x00\x00\x00reheader-101948-02q\x07X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x08sX\x06\x00\x00\x00genomeq\t}q\nX\x06\x00\x00\x00genomeq\x0bX\x0e\x00\x00\x00data/genome.faq\x0csuX\x06\x00\x00\x00outputq\rcsnakemake.io\nOutputFiles\nq\x0e)\x81q\x0fX\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq\x10a}q\x11X\x06\x00\x00\x00_namesq\x12}q\x13sbX\x07\x00\x00\x00threadsq\x14K\x01X\t\x00\x00\x00resourcesq\x15csnakemake.io\nResources\nq\x16)\x81q\x17(K\x01K\x01e}q\x18(X\x06\x00\x00\x00_coresq\x19K\x01h\x12}q\x1a(h\x19K\x00N\x86q\x1bX\x06\x00\x00\x00_nodesq\x1cK\x01N\x86q\x1duh\x1cK\x01ubX\x04\x00\x00\x00ruleq\x1eX\n\x00\x00\x00replace_rgq\x1fX\x06\x00\x00\x00paramsq csnakemake.io\nParams\nq!)\x81q"}q#h\x12}q$sbX\t\x00\x00\x00wildcardsq%csnakemake.io\nWildcards\nq&)\x81q\'X\x12\x00\x00\x00reheader-101948-02q(a}q)(h\x12}q*X\x06\x00\x00\x00sampleq+K\x00N\x86q,sX\x06\x00\x00\x00sampleq-h(ubX\x03\x00\x00\x00logq.csnakemake.io\nLog\nq/)\x81q0X-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq1a}q2h\x12}q3sbX\x05\x00\x00\x00inputq4csnakemake.io\nInputFiles\nq5)\x81q6XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq7a}q8h\x12}q9sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
