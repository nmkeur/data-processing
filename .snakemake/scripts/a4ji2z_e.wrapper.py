
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\t\x00\x00\x00wildcardsq\x03csnakemake.io\nWildcards\nq\x04)\x81q\x05X\x12\x00\x00\x00reheader-101948-02q\x06a}q\x07(X\x06\x00\x00\x00sampleq\x08h\x06X\x06\x00\x00\x00_namesq\t}q\nX\x06\x00\x00\x00sampleq\x0bK\x00N\x86q\x0csubX\t\x00\x00\x00resourcesq\rcsnakemake.io\nResources\nq\x0e)\x81q\x0f(K\x01K\x01e}q\x10(X\x06\x00\x00\x00_nodesq\x11K\x01X\x06\x00\x00\x00_coresq\x12K\x01h\t}q\x13(h\x11K\x00N\x86q\x14h\x12K\x01N\x86q\x15uubX\x05\x00\x00\x00inputq\x16csnakemake.io\nInputFiles\nq\x17)\x81q\x18XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x19a}q\x1ah\t}q\x1bsbX\x07\x00\x00\x00threadsq\x1cK\x01X\x03\x00\x00\x00logq\x1dcsnakemake.io\nLog\nq\x1e)\x81q\x1fX-\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.logq a}q!h\t}q"sbX\x04\x00\x00\x00ruleq#X\n\x00\x00\x00replace_rgq$X\x06\x00\x00\x00configq%}q&(X\x06\x00\x00\x00genomeq\'}q(X\x06\x00\x00\x00genomeq)X\x0e\x00\x00\x00data/genome.faq*sX\x07\x00\x00\x00samplesq+}q,X\x12\x00\x00\x00reheader-101948-02q-X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq.suX\x06\x00\x00\x00outputq/csnakemake.io\nOutputFiles\nq0)\x81q1X\x1f\x00\x00\x00fixed-rg/reheader-101948-02.bamq2a}q3h\t}q4sbX\x06\x00\x00\x00paramsq5csnakemake.io\nParams\nq6)\x81q7}q8h\t}q9sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
