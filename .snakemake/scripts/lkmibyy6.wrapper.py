
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial3/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x04\x00\x00\x00ruleq\x03X\n\x00\x00\x00replace_rgq\x04X\x07\x00\x00\x00threadsq\x05K\x08X\t\x00\x00\x00resourcesq\x06csnakemake.io\nResources\nq\x07)\x81q\x08(K\x08K\x01e}q\t(X\x06\x00\x00\x00_nodesq\nK\x01X\x06\x00\x00\x00_namesq\x0b}q\x0c(X\x06\x00\x00\x00_coresq\rK\x00N\x86q\x0eh\nK\x01N\x86q\x0fuh\rK\x08ubX\t\x00\x00\x00wildcardsq\x10csnakemake.io\nWildcards\nq\x11)\x81q\x12X\x01\x00\x00\x00Bq\x13a}q\x14(h\x0b}q\x15X\x06\x00\x00\x00sampleq\x16K\x00N\x86q\x17sX\x06\x00\x00\x00sampleq\x18h\x13ubX\x05\x00\x00\x00inputq\x19csnakemake.io\nInputFiles\nq\x1a)\x81q\x1bX\x12\x00\x00\x00data/samples/B.bamq\x1ca}q\x1dh\x0b}q\x1esbX\x06\x00\x00\x00outputq\x1fcsnakemake.io\nOutputFiles\nq )\x81q!X\x0e\x00\x00\x00fixed-rg/B.bamq"a}q#h\x0b}q$sbX\x03\x00\x00\x00logq%csnakemake.io\nLog\nq&)\x81q\'X\x1c\x00\x00\x00logs/picard/replace_rg/B.logq(a}q)h\x0b}q*sbX\x06\x00\x00\x00configq+}q,(X\x06\x00\x00\x00genomeq-X\x06\x00\x00\x00data/Aq.X\x03\x00\x00\x00extq/X\x03\x00\x00\x00.faq0X\x07\x00\x00\x00samplesq1}q2(X\x01\x00\x00\x00Aq3X\x0b\x00\x00\x00/data/A.bamq4X\x01\x00\x00\x00Cq5X\x0b\x00\x00\x00/data/C.bamq6h\x13X\x0b\x00\x00\x00/data/B.bamq7uuX\x06\x00\x00\x00paramsq8csnakemake.io\nParams\nq9)\x81q:X_\x00\x00\x00RGID=B RGLB=lib1 RGPL=illumina RGPU=B RGSM=B SORT_ORDER=coordinate VALIDATION_STRINGENCY=SILENTq;a}q<h\x0b}q=sbub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
