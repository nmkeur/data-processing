
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/NIKE/miniconda3/envs/snakemake-tutorial/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00outputq\x03csnakemake.io\nOutputFiles\nq\x04)\x81q\x05X#\x00\x00\x00fixed-rg/reheader-101948-02.bam.bamq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x06\x00\x00\x00paramsq\ncsnakemake.io\nParams\nq\x0b)\x81q\x0c}q\rh\x08}q\x0esbX\x05\x00\x00\x00inputq\x0fcsnakemake.io\nInputFiles\nq\x10)\x81q\x11XA\x00\x00\x00/mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq\x12a}q\x13h\x08}q\x14sbX\t\x00\x00\x00wildcardsq\x15csnakemake.io\nWildcards\nq\x16)\x81q\x17}q\x18h\x08}q\x19sbX\x03\x00\x00\x00logq\x1acsnakemake.io\nLog\nq\x1b)\x81q\x1cX1\x00\x00\x00logs/picard/replace_rg/reheader-101948-02.bam.logq\x1da}q\x1eh\x08}q\x1fsbX\t\x00\x00\x00resourcesq csnakemake.io\nResources\nq!)\x81q"(K\x01K\x01e}q#(X\x06\x00\x00\x00_coresq$K\x01X\x06\x00\x00\x00_nodesq%K\x01h\x08}q&(h%K\x00N\x86q\'h$K\x01N\x86q(uubX\x06\x00\x00\x00configq)}q*(X\x07\x00\x00\x00samplesq+}q,X\x12\x00\x00\x00reheader-101948-02q-X@\x00\x00\x00mnt/WORKSPACE/nike_workspace/Cucumber/BAM/reheader-101948-02.bamq.sX\x06\x00\x00\x00genomeq/}q0X\x06\x00\x00\x00genomeq1X\x0e\x00\x00\x00data/genome.faq2suX\x07\x00\x00\x00threadsq3K\x01X\x04\x00\x00\x00ruleq4X\n\x00\x00\x00replace_rgq5ub.')
######## Original script #########
__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


from snakemake.shell import shell


shell("picard AddOrReplaceReadGroups {snakemake.params} I={snakemake.input} "
      "O={snakemake.output} &> {snakemake.log}")
