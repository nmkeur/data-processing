# Skeleton for the Snakemake workflow

![Workflow overview](/images/dag.svg)

######This repository hosts the code needed for the workflow for data processing.

This workflow starts with BAM files and a fasta reference. First a sequence dictionary and index are created,
Read groups are added to all the BAM files and sorted on postion and an index is created for each BAM. 
Next variants are called on the sorted BAM this result in a VCF(Variant Calling Format) file for each BAM.
Once all sample have been processed all the VCF files will be merged into 1 VCF. Next step is the 
genotyping of this new files and the creation of a summary of all the called variants.

For simplicity an example data-set has been added to the workflow.


```diff
Required software:
* Conda
* GATK (included in this repository)
```

Create the environment and install all necessary tools to run the pipeline.

**This command creates a new environment with the name data-processing.**
```
conda env create -f environment.yaml
```
Activate the environment
```
source activate data-processing
```

In the config.yaml you can set the name and directory to the samples and the
reference fasta need for the pipeline. 



**Genome**: Name and path to reference without extension.

**ext**: Extension of the reference file (.fa/.fasta).

**samples**: Samples used in the pipeline.

```
genome: data/A
ext : .fa
samples:
    A : /data/A.bam
    B : /data/B.bam
    C : /data/C.bam
    D : /data/D.bam
    E : /data/E.bam
    F : /data/F.bam
    G : /data/G.bam
    H : /data/H.bam
    I : /data/I.bam
```

Once the environment has been activated and the config has the required input.
You can start the workflow by simply typing 
```
snakemake
```

Following short description of all the rules used in this pipeline.
Although most of the time the name is already self explanatory.

```
rule build_fasta_index
```
This rule calls "samtools" to create an index for the reference file.
The fasta index file allows efficient random access to the reference bases.

```
rule build_fasta_dictionary
```
This rule calls "picard tools" to create an dictionary for the reference file.
The dictionary is used ass safety check and contains contig names and sizes. 

```
rule replace_rg
```
This rule calls "picard tools" and adds new readgroups to the BAM files.
These readgroups are used to identfy each sample and the same rule
also sort all the reads in the BAM files.

```
rule build_bam_index
```
This rule calls "picard tools" and create an dictionary for the each sample.
The index file allows efficient random access.

```
rule call_variants:
```
This rules uses "GATK" HaplotypeCaller 
to call variants on all samples in GVCF mode.

```
rule merge_variants:
```
This rules used "GATK merge variants" to merge all the vcf files
created by the call variants rule.

```
rule genotype_variants:
```
This rules  runs the "GenotypeGVCFs" from the GATK tools.
To genotype the GVCF files with all merged calls.
```
create_summary
```
This rule will call "bcftools" to create a summary
file based on the fully merged and genotyped vcf.
